const express = require('express');
const router = express.Router();

router.get("/", (req, res)=>{
    res.render('index.ejs');
});


router.get('*', (req, res)=>{
    res.end("no encontrado");
});

module.exports = router;