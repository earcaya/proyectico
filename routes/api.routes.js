const express = require('express');
const router = express.Router();
const fs = require('fs');
const itunesSearchApi = require('itunes-search-api');

router.get('/', async (req, res) => {
   
    const opts = {
        query: {
            entity: 'musicTrack',
            limit: req.query.limit
        }
    };

    itunesSearchApi(req.query.busqueda, opts).then(response => {
        console.log(response.body);
        res.json(response.body);
    })
    .catch(err => {
        //Estatus de error
        res.statusCode = 500;
        res.send({message: "Ha ocurrido un error", err});
    });
});

module.exports = router;