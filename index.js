const express = require('express');
const morgan = require('morgan');
const ejs = require('ejs');
const app = express();
const routes = require('./routes/routes');
const apiRoute = require('./routes/api.routes');
//----------------------------------setting-------------------------------

app.set('appName', 'app tarea web');
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

//----------------------------rutas---------------------------------
app.use(express.static(__dirname + '/public'));
app.use('/api', apiRoute);
app.use(routes);

 
app.listen(3000, ()=>{
    console.log("servidor funcionando!");
    console.log("nombre de la App: " + app.get('appName'));
});